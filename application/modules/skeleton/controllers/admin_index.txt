<h1>MODULE_TITLE Admin</h1>
<p>
This is the index view page for the MODULE_NAME Module Administration.
</p>
<p>
Here you will put all the administration tasks for the MODULE_NAME module.
</p>
<p>
This file is located in /application/MODULE_NAME/views/admin_index.phtml file
</p>
<p>
Access to this administration file is left to the user, or you may remove the
administration component completely, simply by deleting this file and the
MODULE_NAME_admincontroller.php file.
</p>
